package com.example.starter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.spi.json.JsonCodec;
import io.vertx.mysqlclient.MySQLConnectOptions;
import io.vertx.mysqlclient.MySQLPool;
import io.vertx.sqlclient.PoolOptions;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;
import io.vertx.sqlclient.Tuple;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;

public class MysqlVerticle extends AbstractVerticle {
  private final String db_user = "hpsn";
  private final String db_password = "Hpsn6666!";
  private final String db_host = "106.52.28.79";
  private final String db_database = "hpsn";
  private final int db_port = 3306;
  private final String eventBusName = "mysql.bus";


  private MySQLConnectOptions connectOptions = null;
  private PoolOptions poolOptions = null;
  private MySQLPool client = null;

  private HashMap<db_queries, String> DBSQL = null;

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    EventBus eb = vertx.eventBus();
    eb.consumer(eventBusName, message -> {
      handleMessage(message);
    });

    connectOptions = new MySQLConnectOptions()
      .setUser(db_user)
      .setPassword(db_password)
      .setPort(db_port)
      .setHost(db_host)
      .setDatabase(db_database);
    poolOptions = new PoolOptions().setMaxSize(10);
    client = MySQLPool.pool(vertx, connectOptions, poolOptions);

    client
      .getConnection()
      .onSuccess(connection -> {
        Utils.getDBqueries().onSuccess(ar -> {
          DBSQL = ar;
          connection.close();
          startPromise.complete();
        });
      });
  }

  /**
   * 分发信息
   *
   * @param tMessage
   */
  private void handleMessage(Message tMessage) {
    String index = tMessage.headers().get("index");
    switch (index) {
      case "store_path":
        System.out.println("保存图片");
        JsonObject temp = (JsonObject) tMessage.body();
        store_path(temp.getString("zitie_path"), temp.getString("linmo_path"), temp.getString("uuid"), temp.getString("diff_path")).onSuccess(ar -> {
          tMessage.reply("successfully");
        });
        break;
      case "getRemark":
        JsonObject tmp = (JsonObject) tMessage.body();
        System.out.println("获取评分: " + tmp.getString("uuid"));
        getRemark(tmp.getString("uuid"), tMessage);
        break;
      case "zitie_collect_add":
        this.zitie_collect_add((JsonObject) tMessage.body(), tMessage);
        break;
      case "zitie_collect_delete":
        this.zitie_collect_delete((JsonObject) tMessage.body(), tMessage);
        break;
      case "zitie_collect_get":
        this.zitie_collect_get((JsonObject) tMessage.body(), tMessage);
        break;
      case "zitie_collect_exist":
        this.zitie_collect_exist((JsonObject) tMessage.body(), tMessage);
        break;
      case "zitie_collect_exist_total":
        this.zitie_collect_exist_total((JsonObject) tMessage.body(), tMessage);
        break;
      case "shufa_collect_add":
        this.shufa_collect_add((JsonObject) tMessage.body(), tMessage);
        break;
      case "shufa_collect_delete":
        this.shufa_collect_delete((JsonObject) tMessage.body(), tMessage);
        break;
      case "shufa_collect_get":
        this.shufa_collect_get((JsonObject) tMessage.body(), tMessage);
        break;
      case "shufa_collect_exist":
        this.shufa_collect_exist((JsonObject) tMessage.body(), tMessage);
        break;
      case "shufa_collect_exist_total":
        this.shufa_collect_exist_total((JsonObject) tMessage.body(), tMessage);
        break;
      case "goods_collect_add":
        this.goods_collect_add((JsonObject) tMessage.body(), tMessage);
        break;
      case "goods_collect_delete":
        this.goods_collect_delete((JsonObject) tMessage.body(), tMessage);
        break;
      case "goods_collect_get":
        this.goods_collect_get((JsonObject) tMessage.body(), tMessage);
        break;
      case "goods_collect_exist":
        this.goods_collect_exist((JsonObject) tMessage.body(), tMessage);
        break;
      case "goods_collect_exist_total":
        this.goods_collect_exist_total((JsonObject) tMessage.body(), tMessage);
        break;
      case "remark_get_total":
        this.remark_get_total((JsonObject) tMessage.body(), tMessage);
        break;
      case "remark_get_max_score":
        this.remark_get_max_score((JsonObject) tMessage.body(), tMessage);
        break;
      case "remark_get_min_score":
        this.remark_get_min_score((JsonObject) tMessage.body(), tMessage);
        break;
      case "remark_get_avg_score":
        this.remark_get_avg_score((JsonObject) tMessage.body(), tMessage);
        break;
      case "assess_info_insert":
        this.assess_info_insert((JsonObject) tMessage.body(), tMessage);
        break;
      case "assess_info_getbyid":
        this.assess_info_getbyid((JsonObject) tMessage.body(), tMessage);
        break;
      default:
        tMessage.fail(0, "error: not such service in MysqlVerticle");
        System.out.println("error: not such service in MysqlVerticle");
    }
  }


  /**
   * 保存对应的路径
   *
   * @param zitie_path
   * @param linmo_path
   * @param uid
   */
  private Future<Void> store_path(String zitie_path, String linmo_path, String uid, String diff_path) {
    Promise<Void> promise = Promise.promise();
    client.getConnection().onSuccess(conn -> {
      conn
        .preparedQuery("insert into assess_record (ZiTie_path, LinMo_path, uuid, Diff_path) values (?, ?, ?, ?)")
        .execute(Tuple.of(zitie_path, linmo_path, uid, diff_path), ar -> {
          conn.close();
          if (ar.succeeded()) {
//            查询成功
            promise.complete();
          } else {
            System.out.println(ar.cause());
//            查询失败
            promise.fail(ar.cause());
          }
        });
    });
//    返回结果
    return promise.future();
  }

  /**
   * 获取差异评论与分数
   *
   * @param uuid
   * @param tMessage
   * @return
   */
  private Future<Void> getRemark(String uuid, Message tMessage) {
    System.out.println("into getRemark:" + uuid);
    Promise<Void> promise = Promise.promise();
    client.getConnection().onSuccess(conn -> {
      System.out.println("从连接池中获取连接");
      conn
        .preparedQuery("select * from assess_result where uuid = ?")
        .execute(Tuple.of(uuid), ar -> {
          conn.close();
          if (ar.succeeded()) {
            RowSet<Row> result = ar.result();
            if (result.size() == 0) {
              tMessage.fail(1, "数据库中没有该选项");
              promise.fail(ar.cause());
            }
            for (Row row : result) {
              JsonObject data = new JsonObject().put("score", row.getNumeric("score")).put("comment", row.getString("comment")).put("uuid", row.getString("uuid"));
              tMessage.reply(data);
              promise.complete();
            }
          } else {
            System.out.println(ar.cause());
            tMessage.fail(1, ar.cause().toString());
            promise.fail(ar.cause());
          }
        });

    });
    return promise.future();
  }


  /**
   * 添加字帖收藏
   *
   * @param data
   * @param tMessage
   */
  private void zitie_collect_add(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    String id = data.getString("id");
    client
      .getConnection()
      .onSuccess(conn -> {
        conn
          .preparedQuery(DBSQL.get(db_queries.ZITIE_COLLECT_ADD))
          .execute(Tuple.of(openid, id), ar -> {
            if (ar.succeeded()) {
              tMessage.reply("zitie_collect_add of openid: " + openid + " zitie_index: " + id + " success");
            } else {
              tMessage.fail(0, ar.cause().toString());
            }
            conn.close();
          });
      });
  }

  /**
   * 删除字帖收藏
   *
   * @param data
   * @param tMessage
   */
  private void zitie_collect_delete(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    String id = data.getString("id");
    client
      .getConnection()
      .onSuccess(conn -> {
        conn
          .preparedQuery(DBSQL.get(db_queries.ZITIE_COLLECT_DELETE))
          .execute(Tuple.of(openid, id), ar -> {
            conn.close();
            if (ar.succeeded()) {
              tMessage.reply("zitie_collect_delete of openid: " + openid + " zitie_index: " + id + " success");
            } else {
              tMessage.fail(0, ar.cause().toString());
            }
          });
      });
  }

  /**
   * 获取用户收藏的字帖
   *
   * @param data
   * @param tMeesage
   */
  private void zitie_collect_get(JsonObject data, Message tMeesage) {
    String openid = data.getString("openid");
    client
      .getConnection()
      .onSuccess(connection -> {
        connection
          .preparedQuery(DBSQL.get(db_queries.ZITIE_COLLECT_GET))
          .execute(Tuple.of(openid))
          .onSuccess(res -> {
            RowSet<Row> rowSet = res;
            JsonArray result = new JsonArray();
            for (Row row : rowSet) {
              result.add(
                new JsonObject()
                  .put("id", row.getString(0))
                  .put("pic_url", row.getString(1))
                  .put("name", row.getString(2))
                  .put("author", row.getString(3))
                  .put("kind", row.getString(4))
              );
            }
            tMeesage.reply(result);
          })
          .onFailure(f -> tMeesage.fail(0, f.toString()))
          .eventually(v -> connection.close());
      });
  }

  /**
   * 判断字帖收藏中是否存在某条记录
   *
   * @param data
   * @param tMessage
   */
  private void zitie_collect_exist(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    String zitie_index = data.getString("id");
    client
      .getConnection()
      .onSuccess(connection -> {
        connection
          .preparedQuery(DBSQL.get(db_queries.ZITIE_COLLECT_EXIST))
          .execute(Tuple.of(openid, zitie_index))
          .onSuccess(res -> {
            RowSet<Row> rowSet = res;
            Iterator it = rowSet.iterator();
            if (it.hasNext()) {
              Row temp = (Row) it.next();
              if (temp.getInteger(0) == 1) {
                //  存在
                tMessage.reply(true);
              } else {
                //  不存在
                tMessage.reply(false);
              }
            }
          })
          .onFailure(f -> {
            tMessage.fail(0, f.toString());
          })
          .eventually(v -> connection.close());
      });
  }

  /**
   * 统计某用户收藏字帖的数量
   *
   * @param data
   * @param tMessage
   */
  private void zitie_collect_exist_total(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    client
      .getConnection()
      .onSuccess(conn -> {
        conn
          .preparedQuery(DBSQL.get(db_queries.ZITIE_COLLECT_EXIST_TOTAL))
          .execute(Tuple.of(openid))
          .onSuccess(res -> {
            RowSet<Row> rowSet = res;
            Iterator<Row> it = rowSet.iterator();
            tMessage.reply(rowSet.iterator().next().getInteger(0));
          })
          .onFailure(f -> {
            tMessage.fail(0, f.getCause().toString());
          })
          .eventually(e -> conn.close());
      });
  }

  private void shufa_collect_add(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    String shufa_index = data.getString("id");
    client
      .getConnection()
      .onSuccess(conn -> {
        conn
          .preparedQuery(DBSQL.get(db_queries.SHUFA_COLLECT_ADD))
          .execute(Tuple.of(openid, shufa_index), ar -> {
            conn.close();
            if (ar.succeeded()) {
              tMessage.reply("shufa_collect_add of openid: " + openid + " shufa_index: " + shufa_index + " success");
            } else {
              tMessage.fail(0, ar.cause().toString());
            }
          });
      });
  }

  private void shufa_collect_delete(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    String shufa_index = data.getString("id");
    client
      .getConnection()
      .onSuccess(conn -> {
        conn
          .preparedQuery(DBSQL.get(db_queries.SHUFA_COLLECT_DELETE))
          .execute(Tuple.of(openid, shufa_index), ar -> {
            conn.close();
            if (ar.succeeded()) {
              tMessage.reply("shufa_collect_delete of openid: " + openid + " shufa_index: " + shufa_index + " success");
            } else {
              tMessage.fail(0, ar.cause().toString());
            }
          });
      });
  }

  private void shufa_collect_get(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    System.out.println("openid: " + openid);
    client
      .getConnection()
      .onSuccess(conn -> {
        conn
          .preparedQuery(DBSQL.get(db_queries.SHUFA_COLLECT_GET))
          .execute(
            Tuple.of(openid),
            ar -> {
              conn.close();
              if (ar.succeeded()) {
                JsonArray result = new JsonArray();
                RowSet<Row> rowSet = ar.result();
                for (Row row : rowSet) {
                  result.add(
                    new JsonObject()
                      .put("id", row.getString(0))
                      .put("pic_url", row.getString(1))
                      .put("name", row.getString(2))
                  );
                }
                tMessage.reply(result);
              } else {
                tMessage.fail(0, ar.cause().toString());
              }
            });
      });
  }

  /**
   * 查看书法收藏是否存在
   *
   * @param data
   * @param tMessage
   */
  private void shufa_collect_exist(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    String shufa_index = data.getString("id");
    client
      .getConnection()
      .onSuccess(connection -> {
        connection
          .preparedQuery(DBSQL.get(db_queries.SHUFA_COLLECT_EXIST))
          .execute(Tuple.of(openid, shufa_index))
          .onSuccess(res -> {
            RowSet<Row> rowSet = res;
            Iterator it = rowSet.iterator();
            if (it.hasNext()) {
              Row temp = (Row) it.next();
              if (temp.getInteger(0) == 1) {
                //  存在
                tMessage.reply(true);
              } else {
                //  不存在
                tMessage.reply(false);
              }
            }
          })
          .onFailure(f -> {
            tMessage.fail(0, f.toString());
          })
          .eventually(v -> connection.close());
      });
  }

  /**
   * 统计某用户收藏书法的数量
   *
   * @param data
   * @param tMessage
   */
  private void shufa_collect_exist_total(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    client
      .getConnection()
      .onSuccess(
        conn -> {
          conn
            .preparedQuery(DBSQL.get(db_queries.SHUFA_COLLECT_EXIST_TOTAL))
            .execute(Tuple.of(openid))
            .onSuccess(res -> {
              tMessage.reply(res.iterator().next().getInteger(0));
            })
            .onFailure(f -> {
              tMessage.fail(0, f.getCause().toString());
            })
            .eventually(e -> conn.close());
        }
      );
  }

  private void goods_collect_add(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    String goods_index = data.getString("id");
    client
      .getConnection()
      .onSuccess(conn -> {
        conn
          .preparedQuery(DBSQL.get(db_queries.GOODS_COLLECT_ADD))
          .execute(Tuple.of(openid, goods_index), ar -> {
            conn.close();
            if (ar.succeeded()) {
              tMessage.reply("goods_collect_add of openid: " + openid + " goods_index: " + goods_index + " success");
            } else {
              tMessage.fail(0, ar.cause().toString());
            }
          });
      });
  }

  private void goods_collect_delete(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    String goods_index = data.getString("id");
    client
      .getConnection()
      .onSuccess(conn -> {
        conn
          .preparedQuery(DBSQL.get(db_queries.GOODS_COLLECT_DELETE))
          .execute(Tuple.of(openid, goods_index), ar -> {
            conn.close();
            if (ar.succeeded()) {
              tMessage.reply("goods_collect_delete of openid: " + openid + " goods_index: " + goods_index + " success");
            } else {
              tMessage.fail(0, ar.cause().toString());
            }
          });
      });
  }

  private void goods_collect_get(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    client
      .getConnection()
      .onSuccess(conn -> {
        conn
          .preparedQuery(DBSQL.get(db_queries.GOODS_COLLECT_GET))
          .execute(
            Tuple.of(openid),
            ar -> {
              conn.close();
              if (ar.succeeded()) {
                JsonArray result = new JsonArray();
                RowSet<Row> rowSet = ar.result();
                for (Row row : rowSet) {
                  result.add(
                    new JsonObject()
                      .put("id", row.getString(0))
                      .put("pic_url", row.getString(1))
                      .put("name", row.getString(2))
                  );
                }
                tMessage.reply(result);
              } else {
                tMessage.fail(0, ar.cause().toString());
              }
            });
      });
  }

  /**
   * 查看商品收藏是否存在
   *
   * @param data
   * @param tMessage
   */
  private void goods_collect_exist(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    String goods_index = data.getString("id");
    client
      .getConnection()
      .onSuccess(connection -> {
        connection
          .preparedQuery(DBSQL.get(db_queries.GOODS_COLLECT_EXIST))
          .execute(Tuple.of(openid, goods_index))
          .onSuccess(res -> {
            System.out.println(res.toString());
            RowSet<Row> rowSet = res;
            Iterator it = rowSet.iterator();
            if (it.hasNext()) {
              Row temp = (Row) it.next();
              if (temp.getInteger(0) == 1) {
                //  存在
                tMessage.reply(true);
              } else {
                //  不存在
                tMessage.reply(false);
              }
            }
          })
          .onFailure(f -> {
            tMessage.fail(0, f.toString());
          })
          .eventually(v -> connection.close());
      });
  }

  /**
   * 统计某用户收藏商品数量
   *
   * @param data
   * @param tMessage
   */
  private void goods_collect_exist_total(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    client
      .getConnection()
      .onSuccess(
        conn -> {
          conn
            .preparedQuery(DBSQL.get(db_queries.GOODS_COLLECT_EXIST_TOTAL))
            .execute(Tuple.of(openid))
            .onSuccess(res -> {
              tMessage.reply(res.iterator().next().getInteger(0));
            })
            .onFailure(f -> {
              tMessage.fail(0, f.getCause().toString());
            })
            .eventually(e -> conn.close());
        }
      );
  }

  /**
   * 获取某用户的所有评价记录
   *
   * @param data
   * @param tMessage
   */
  private void remark_get_total(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    client
      .getConnection()
      .onSuccess(
        conn -> {
          conn
            .preparedQuery(DBSQL.get(db_queries.REMARK_GET_TOTAL))
            .execute(Tuple.of(openid))
            .onSuccess(res -> {
              RowSet<Row> rowSet = res;
              Iterator<Row> it = rowSet.iterator();
              if (rowSet.size() == 0) {
                tMessage.reply(new JsonObject().put("data", null));
              } else {
                JsonArray result = new JsonArray();
                while (it.hasNext()) {
                  Row tmp = it.next();
                  result.add(
                    new JsonObject()
                      .put("score", tmp.getDouble(0))
                      .put("comment", tmp.getString(1))
                      .put("uuid", tmp.getString(2))
                  );
                }
                tMessage.reply(new JsonObject().put("data", result));
              }
            })
            .onFailure(f -> {
              tMessage.fail(0, f.getCause().toString());
            })
            .eventually(e -> conn.close());
        }
      );
  }

  /**
   * 获取某用户最高评分的记录
   *
   * @param data
   * @param tMessage
   */
  private void remark_get_max_score(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    client
      .getConnection()
      .onSuccess(
        conn -> {
          conn
            .preparedQuery(DBSQL.get(db_queries.REMARK_GET_MAX_SCORE))
            .execute(Tuple.of(openid, openid))
            .onSuccess(res -> {
              RowSet<Row> rowSet = res;
              if (rowSet.size() == 0) {
                tMessage.reply(new JsonObject().put("data", null));
              } else {
                Iterator<Row> it = rowSet.iterator();
                JsonArray result = new JsonArray();
                while (it.hasNext()) {
                  Row tmp = it.next();
                  result.add(
                    new JsonObject()
                      .put("score", tmp.getDouble(0))
                      .put("comment", tmp.getString(1))
                      .put("uuid", tmp.getString(2))
                  );
                }
                tMessage.reply(new JsonObject().put("data", result));
              }
            })
            .onFailure(f -> {
              tMessage.fail(0, f.getCause().toString());
            })
            .eventually(e -> conn.close());
        }
      );
  }

  private void remark_get_min_score(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    client
      .getConnection()
      .onSuccess(
        conn -> {
          conn
            .preparedQuery(DBSQL.get(db_queries.REMARK_GET_MIN_SCORE))
            .execute(Tuple.of(openid, openid))
            .onSuccess(res -> {
              RowSet<Row> rowSet = res;
              if (rowSet.size() == 0) {
                tMessage.reply(new JsonObject().put("data", null));
              } else {
                Iterator<Row> it = rowSet.iterator();
                JsonArray result = new JsonArray();
                while (it.hasNext()) {
                  Row tmp = it.next();
                  result.add(
                    new JsonObject()
                      .put("score", tmp.getDouble(0))
                      .put("comment", tmp.getString(1))
                      .put("uuid", tmp.getString(2))
                  );
                }
                tMessage.reply(new JsonObject().put("data", result));
              }
            })
            .onFailure(f -> {
              tMessage.fail(0, f.getCause().toString());
            })
            .eventually(e -> conn.close());
        }
      );
  }

  private void remark_get_avg_score(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    client
      .getConnection()
      .onSuccess(
        conn -> {
          conn
            .preparedQuery(DBSQL.get(db_queries.REMARK_GET_AVG_SCORE))
            .execute(Tuple.of(openid))
            .onSuccess(res -> {
              RowSet<Row> rowSet = res;
              tMessage.reply(new JsonObject().put("avg_score", rowSet.iterator().next().getDouble(0)));
            })
            .onFailure(f -> {
              tMessage.fail(0, f.getCause().toString());
            })
            .eventually(e -> conn.close());
        }
      );
  }

  private void assess_info_insert(JsonObject data, Message tMessage) {
    String openid = data.getString("openid");
    String uuid = data.getString("uuid");
    String id = data.getString("id");
    client
      .getConnection()
      .onSuccess(
        conn -> {
          conn
            .preparedQuery(DBSQL.get(db_queries.ASSESS_INFO_INSERT))
            .execute(Tuple.of(openid, uuid, id))
            .onSuccess(res -> {
              tMessage.reply(null);
            })
            .onFailure(f -> {
              tMessage.fail(0, f.getCause().toString());
            })
            .eventually(e -> conn.close());
        }
      );
  }

  /**
   * 获取特定id字帖的左右评价记录
   *
   * @param data
   * @param tMessage
   */
  private void assess_info_getbyid(JsonObject data, Message tMessage) {
    String id = data.getString("id");
    client
      .getConnection()
      .onSuccess(
        conn -> {
          conn
            .preparedQuery(DBSQL.get(db_queries.ASSESS_INFO_GETBYID))
            .execute(Tuple.of(id))
            .onSuccess(
              res -> {
                if (res.size() == 0) {
                  tMessage.reply(new JsonObject().put("data", null));
                } else {
                  JsonArray result = new JsonArray();
                  RowSet<Row> rowSet = res;
                  Iterator<Row> it = rowSet.iterator();
                  while (it.hasNext()) {
                    Row tmp = it.next();
                    result.add(
                      new JsonObject()
                        .put("score", tmp.getDouble(0))
                        .put("comment", tmp.getString(1))
                        .put("uuid", tmp.getString(2))
                        .put("openid", tmp.getString(3))
                    );
                  }
                  tMessage.reply(new JsonObject().put("data", result));
                }
              }
            )
            .onFailure(
              f -> {
                tMessage.fail(0, f.getCause().toString());
              }
            )
            .eventually(e -> conn.close());
        }
      );
  }

}
