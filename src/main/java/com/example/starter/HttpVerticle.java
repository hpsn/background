package com.example.starter;


import io.vertx.core.*;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.file.CopyOptions;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerAdapter;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.json.impl.JsonUtil;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.BodyHandler;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class HttpVerticle extends AbstractVerticle {

  private static final Logger LOGGER = LoggerFactory.getLogger(HttpVerticle.class);
  private static final String APPID = "wx3d588e6f83def48a";
  private static final String SECRET = "dc3200fbab461f247f58d4f6525dae69";
  private static String session_key = null;
  private static String openid = null;
  private static String uploadPIC_rootPath = null;
  private static String standPIC_rootPath = null;

  private EventBus eventBus = null;


  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    eventBus = vertx.eventBus();

    HttpServer server = vertx.createHttpServer();
    Router router = Router.router(vertx);
    router.post().handler(BodyHandler.create());
    //    接受图片
    router.post("/image").handler(this::image);
    router.get("/image").handler(this::image);
    router.get("/index").handler(this::index);
    router.post("/onLogin").handler(this::onLogin);
    router.get("/templates").handler(this::getTemplates);
    router.get("/template").handler(this::getTemplate);
    //    get the result image including linmo, template, wan_mark.
    //    获取临摹图像
    router.get("/linmo").handler(this::linmoIMG);
    //    获取标准图像
    router.get("/template_2").handler(this::templateIMG);
    //    获取差异图像
    router.get("/wan_mark").handler(this::wan_markIMG);

    router.get("/remark").handler(this::getRemark);

    //  字帖收藏功能
    router.post("/zitie_collect/add").handler(this::zitie_collect_add);
    router.post("/zitie_collect/delete").handler(this::zitie_collect_delete);
    router.get("/zitie_collect/get").handler(this::zitie_collect_get);
    router.get("/zitie_collect/exist").handler(this::zitie_collect_exist);
    router.get("/zitie_collect/exist/total").handler(this::zitie_collect_exist_total);
    //  书法收藏功能
    router.post("/shufa_collect/add").handler(this::shufa_collect_add);
    router.post("/shufa_collect/delete").handler(this::shufa_collect_delete);
    router.get("/shufa_collect/get").handler(this::shufa_collect_get);
    router.get("/shufa_collect/exist").handler(this::shufa_collect_exist);
    router.get("/shufa_collect/exist/total").handler(this::shufa_collect_exist_total);
    //  商品收藏功能
    router.post("/goods_collect/add").handler(this::goods_collect_add);
    router.post("/goods_collect/delete").handler(this::goods_collect_delete);
    router.get("/goods_collect/get").handler(this::goods_collect_get);
    router.get("/goods_collect/exist").handler(this::goods_collect_exist);
    router.get("/goods_collect/exist/total").handler(this::goods_collect_exist_total);
    //  评论统计功能
    router.get("/remark/total").handler(this::remark_get_total);
    router.get("/remark/max/score").handler(this::remark_get_max_score);
    router.get("/remark/min/score").handler(this::remark_get_min_score);
    router.get("/remark/avg/score").handler(this::remark_get_avg_score);
    router.get("/remark/get/byid").handler(this::assess_info_getbyid);

    server.requestHandler(router).listen(8888, ar -> {
      if (ar.succeeded()) {
        LOGGER.info("HTTP server start on port " + ar.result().actualPort());
        startPromise.complete();
      } else {
//        LOGGER.error(ar.cause());
        server.requestHandler(router).listen(8000, ar2 -> {
          if (ar2.succeeded()) {
            LOGGER.info("HTTP server start on port " + ar2.result().actualPort());
            startPromise.complete();
          } else {
            LOGGER.error("can not start HTTP");
            startPromise.fail(ar.cause());
          }
        });
      }
    });
  }

  /**
   * 添加字帖收藏
   *
   * @param routingContext
   */
  private void zitie_collect_add(RoutingContext routingContext) {
    System.out.println("into http service");
    JsonObject res = routingContext.getBodyAsJson();
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "zitie_collect_add");
    eventBus.request("mysql.bus", res, options, reply -> {
      if (reply.succeeded()) {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject()
            .put("error_code", 0)
            .put("message", "success")
            .encodePrettily());
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject()
            .put("error_code", 1)
            .put("message", reply.cause())
            .encodePrettily());
      }
    });
  }

  /**
   * 删除字帖收藏
   *
   * @param routingContext
   */
  private void zitie_collect_delete(RoutingContext routingContext) {
    JsonObject res = routingContext.getBodyAsJson();
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "zitie_collect_delete");
    eventBus.request("mysql.bus", res, options, reply -> {
      if (reply.succeeded()) {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject()
            .put("error_code", 0)
            .put("message", "success")
            .encodePrettily());
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject()
            .put("error_code", 1)
            .put("message", "fail")
            .encodePrettily());
      }
    });
  }

  /**
   * 获取用户字帖收藏
   *
   * @param routingContext
   */
  private void zitie_collect_get(RoutingContext routingContext) {
    String openid = routingContext.request().getParam("openid");
    JsonObject data = new JsonObject();
    data.put("openid", openid);
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "zitie_collect_get");
    eventBus.request("mysql.bus", data, options, reply -> {
      if (reply.succeeded()) {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("error_code", 0)
              .put("message", "success")
              .put("data", reply.result().body()).encodePrettily()
          );
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject()
            .put("error_code", 1)
            .put("message", "fail")
            .encodePrettily());
      }
    });
  }

  private void zitie_collect_exist(RoutingContext routingContext) {
    String openid = routingContext.request().getParam("openid");
    String id = routingContext.request().getParam("id");
    JsonObject data = new JsonObject();
    data
      .put("openid", openid)
      .put("id", id);
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "zitie_collect_exist"), reply -> {
      if (reply.succeeded()) {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("openid", openid)
              .put("id", id)
              .put("isexist", reply.result().body())
              .encodePrettily()
          );
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject().put("error_code", 1).put("message", reply.cause()).encodePrettily()
          );
      }
    });
  }

  private void zitie_collect_exist_total(RoutingContext routingContext) {
    String openid = routingContext.request().getParam("openid");
    JsonObject data = new JsonObject();
    data.put("openid", openid);
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "zitie_collect_exist_total"), reply -> {
      if (reply.succeeded()) {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("openid", openid)
              .put("count", reply.result().body())
              .encodePrettily()
          );
      } else {
        routingContext
          .response()
          .putHeader("index", "application/json")
          .end(
            new JsonObject()
              .put("error_code", 1)
              .put("message", reply.cause())
              .encodePrettily()
          );
      }
    });
  }


  private void shufa_collect_add(RoutingContext routingContext) {
    JsonObject res = routingContext.getBodyAsJson();
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "shufa_collect_add");
    eventBus.request("mysql.bus", res, options, reply -> {
      if (reply.succeeded()) {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject()
            .put("error_code", 0)
            .put("message", "success")
            .encodePrettily());
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject()
            .put("error_code", 1)
            .put("message", "fail")
            .encodePrettily());
      }
    });
  }

  private void shufa_collect_delete(RoutingContext routingContext) {
    JsonObject res = routingContext.getBodyAsJson();
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "shufa_collect_delete");
    eventBus.request("mysql.bus", res, options, reply -> {
      if (reply.succeeded()) {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject()
            .put("error_code", 0)
            .put("message", "success")
            .encodePrettily());
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject()
            .put("error_code", 1)
            .put("message", "fail")
            .encodePrettily());
      }
    });
  }

  private void shufa_collect_get(RoutingContext routingContext) {
    String openid = routingContext.request().getParam("openid");
    JsonObject data = new JsonObject();
    data.put("openid", openid);
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "shufa_collect_get");
    eventBus.request("mysql.bus", data, options, reply -> {
      if (reply.succeeded()) {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("error_code", 0)
              .put("message", "success")
              .put("data", reply.result().body()).encodePrettily()
          );
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject()
            .put("error_code", 1)
            .put("message", "fail")
            .encodePrettily());
      }
    });
  }

  private void shufa_collect_exist(RoutingContext routingContext) {
    String openid = routingContext.request().getParam("openid");
    String id = routingContext.request().getParam("id");
    JsonObject data = new JsonObject();
    data
      .put("openid", openid)
      .put("id", id);
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "shufa_collect_exist"), reply -> {
      if (reply.succeeded()) {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("openid", openid)
              .put("id", id)
              .put("isexist", reply.result().body())
              .encodePrettily()
          );
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject().put("error_code", 1).put("message", reply.cause()).encodePrettily()
          );
      }
    });
  }

  private void shufa_collect_exist_total(RoutingContext routingContext) {
    String openid = routingContext.request().getParam("openid");
    JsonObject data = new JsonObject();
    data.put("openid", openid);
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "shufa_collect_exist_total"), reply -> {
      if (reply.succeeded()) {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("openid", openid)
              .put("count", reply.result().body())
              .encodePrettily()
          );
      } else {
        routingContext
          .response()
          .putHeader("index", "application/json")
          .end(
            new JsonObject()
              .put("error_code", 1)
              .put("message", reply.cause())
              .encodePrettily()
          );
      }
    });
  }

  private void goods_collect_add(RoutingContext routingContext) {
    JsonObject res = routingContext.getBodyAsJson();
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "goods_collect_add");
    eventBus.request("mysql.bus", res, options, reply -> {
      if (reply.succeeded()) {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject()
            .put("error_code", 0)
            .put("message", "success")
            .encodePrettily());
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject()
            .put("error_code", 1)
            .put("message", "fail")
            .encodePrettily());
      }
    });
  }

  private void goods_collect_delete(RoutingContext routingContext) {
    JsonObject res = routingContext.getBodyAsJson();
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "goods_collect_delete");
    eventBus.request("mysql.bus", res, options, reply -> {
      if (reply.succeeded()) {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject()
            .put("error_code", 0)
            .put("message", "success")
            .encodePrettily());
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject()
            .put("error_code", 1)
            .put("message", "fail")
            .encodePrettily());
      }
    });
  }

  private void goods_collect_get(RoutingContext routingContext) {
    String openid = routingContext.request().getParam("openid");
    JsonObject data = new JsonObject();
    data.put("openid", openid);
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "goods_collect_get");
    eventBus.request("mysql.bus", data, options, reply -> {
      if (reply.succeeded()) {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("error_code", 0)
              .put("message", "success")
              .put("data", reply.result().body()).encodePrettily()
          );
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject()
            .put("error_code", 1)
            .put("message", "fail")
            .encodePrettily());
      }
    });
  }

  private void goods_collect_exist(RoutingContext routingContext) {
    String openid = routingContext.request().getParam("openid");
    String id = routingContext.request().getParam("id");
    JsonObject data = new JsonObject();
    data
      .put("openid", openid)
      .put("id", id);
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "goods_collect_exist"), reply -> {
      if (reply.succeeded()) {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("openid", openid)
              .put("id", id)
              .put("isexist", reply.result().body())
              .encodePrettily()
          );
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject().put("error_code", 1).put("message", reply.cause()).encodePrettily()
          );
      }
    });
  }

  private void goods_collect_exist_total(RoutingContext routingContext) {
    String openid = routingContext.request().getParam("openid");
    JsonObject data = new JsonObject();
    data.put("openid", openid);
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "goods_collect_exist_total"), reply -> {
      if (reply.succeeded()) {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("openid", openid)
              .put("count", reply.result().body())
              .encodePrettily()
          );
      } else {
        routingContext
          .response()
          .putHeader("index", "application/json")
          .end(
            new JsonObject()
              .put("error_code", 1)
              .put("message", reply.cause())
              .encodePrettily()
          );
      }
    });
  }


  private void getRemark(RoutingContext routingContext) {
    String uuid = routingContext.request().getParam("uuid");
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "getRemark");
    JsonObject data = new JsonObject().put("uuid", uuid);
    eventBus.request("mysql.bus", data, options, reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        routingContext.response().putHeader("content-type", "application/json").end(((JsonObject) reply.result().body()).encodePrettily());
//        routingContext.response().putHeader("content-type", "application/json").end("fail");

      } else {
        JsonObject result = new JsonObject().put("err_code", 1).put("data", uuid).put("msg", "fail");
        routingContext.response().putHeader("content-type", "application/json").end(result.encode());
      }
    });
  }

  private void remark_get_total(RoutingContext routingContext) {
    String openid = routingContext.request().getParam("openid");
    eventBus.request("mysql.bus", new JsonObject().put("openid", openid), new DeliveryOptions().addHeader("index", "remark_get_total"), reply -> {
      if (reply.succeeded()) {
        JsonObject tmp = (JsonObject) reply.result().body();
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("openid", openid)
              .put("data", tmp.getJsonArray("data"))
              .encodePrettily()
          );
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("err_code", 1)
              .put("msg", reply.cause())
              .encodePrettily()
          );
      }
    });
  }

  private void remark_get_max_score(RoutingContext routingContext) {
    String openid = routingContext.request().getParam("openid");
    eventBus.request("mysql.bus", new JsonObject().put("openid", openid), new DeliveryOptions().addHeader("index", "remark_get_max_score"), reply -> {
      if (reply.succeeded()) {
        JsonObject tmp = (JsonObject) reply.result().body();
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("openid", openid)
              .put("data", tmp.getJsonArray("data"))
              .encodePrettily()
          );
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("err_code", 1)
              .put("msg", reply.cause())
              .encodePrettily()
          );
      }
    });
  }

  private void remark_get_min_score(RoutingContext routingContext) {
    String openid = routingContext.request().getParam("openid");
    eventBus.request("mysql.bus", new JsonObject().put("openid", openid), new DeliveryOptions().addHeader("index", "remark_get_min_score"), reply -> {
      if (reply.succeeded()) {
        JsonObject tmp = (JsonObject) reply.result().body();
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("openid", openid)
              .put("data", tmp.getJsonArray("data"))
              .encodePrettily()
          );
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("err_code", 1)
              .put("msg", reply.cause())
              .encodePrettily()
          );
      }
    });
  }

  private void remark_get_avg_score(RoutingContext routingContext) {
    String openid = routingContext.request().getParam("openid");
    eventBus.request("mysql.bus", new JsonObject().put("openid", openid), new DeliveryOptions().addHeader("index", "remark_get_avg_score"), reply -> {
      if (reply.succeeded()) {
        JsonObject tmp = (JsonObject) reply.result().body();
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("openid", openid)
              .put("avg_score", tmp.getDouble("avg_score"))
              .encodePrettily()
          );
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(
            new JsonObject()
              .put("err_code", 1)
              .put("msg", reply.cause())
              .encodePrettily()
          );
      }
    });
  }

  /**
   * 获取临摹图片
   *
   * @param routingContext
   */
  private void linmoIMG(RoutingContext routingContext) {
    String uuid = routingContext.request().getParam("uuid");
    HttpVerticle.standPIC_rootPath = System.getProperty("user.dir").toString() + "/test/" + uuid + ".jpg";
    vertx.fileSystem().readFile(HttpVerticle.standPIC_rootPath, res -> {
      if (res.succeeded()) {
        routingContext.response().putHeader("content-type", "image/jpeg").end(res.result());
      } else {
        LOGGER.error(res.cause());
      }
    });
  }

  /**
   * 获取标准图片
   *
   * @param routingContext
   */
  private void templateIMG(RoutingContext routingContext) {
    String index = routingContext.request().getParam("index");
    HttpVerticle.standPIC_rootPath = System.getProperty("user.dir").toString() + "/stand_pictures/" + index + ".jpg";
    vertx.fileSystem().readFile(HttpVerticle.standPIC_rootPath, res -> {
      if (res.succeeded()) {
        routingContext.response().putHeader("content-type", "image/jpeg").end(res.result());
      } else {
        LOGGER.error(res.cause());
      }
    });
  }

  /**
   * 获取差异图像
   *
   * @param routingContext
   */
  private void wan_markIMG(RoutingContext routingContext) {
    String uuid = routingContext.request().getParam("uuid");
    HttpVerticle.standPIC_rootPath = System.getProperty("user.dir").toString() + "/result/" + uuid + ".jpg";
    vertx.fileSystem().readFile(HttpVerticle.standPIC_rootPath, res -> {
      if (res.succeeded()) {
        routingContext.response().putHeader("content-type", "image/jpeg").end(res.result());
      } else {
        LOGGER.error(res.cause());
      }
    });
  }


  /**
   * 获取汉字模板图片
   *
   * @param routingContext
   */
  private void getTemplate(RoutingContext routingContext) {
    String index = routingContext.request().getParam("index");
    HttpVerticle.standPIC_rootPath = System.getProperty("user.dir").toString() + "/stand_pictures/" + index + ".jpg";
    vertx.fileSystem().readFile(HttpVerticle.standPIC_rootPath, res -> {
      if (res.succeeded()) {
        routingContext.response().putHeader("content-type", "image/jpeg").end(res.result());
      } else {
        LOGGER.info("the pic of " + index + " don't exit");
      }
    });
  }

  /**
   * 获取汉字模板图片地址
   *
   * @param routingContext
   */
  private void getTemplates(RoutingContext routingContext) {
    LOGGER.info("get template pictures");
    JsonObject result = new JsonObject();
    JsonArray data = new JsonArray();
    String pic_url = "http://106.52.28.79/template?index=";
    for (int i = 0; i < 20; i++) {
      int index = i;
      data.add(new JsonObject().put("index", Integer.toString(index)).put("pic_url", pic_url + String.format("%04d", index)));
    }
    result.put("errcode", 0);
    result.put("data", data);
    routingContext.response().putHeader("content-type", "application/json").end(result.encode());
  }

  /**
   * 小程序登陆
   *
   * @param routingContext
   */
  private void onLogin(RoutingContext routingContext) {
//    获取前端所发送过来的code（JSON格式）
    JsonObject login_code = routingContext.getBodyAsJson();
    String code = login_code.getString("code");
    LOGGER.info("code: " + code);

//    GET https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code
//    code: 每登陆一次小程序都会动态变化
//    Appid: wxf01a7c90ba669295
//    Appsecret: 0da6e69eea0eb0eed1c9cd0d264f43dd
//    创建WebClient，用于发送HTTP或则HTTPS请求
    WebClient webClient = WebClient.create(vertx);
    webClient
      .getAbs("https://api.weixin.qq.com/sns/jscode2session")
      .addQueryParam("appid", APPID)
      .addQueryParam("secret", SECRET)
      .addQueryParam("js_code", code)
      .addQueryParam("grant_type", "authorization_code")
      .send(handle -> {
//        处理响应结果
        if (handle.succeeded()) {
//          这里拿到的结果就是一个HTML文本，直接日志输出
          session_key = handle.result().bodyAsJsonObject().getString("session_key");
          openid = handle.result().bodyAsJsonObject().getString("openid");
          LOGGER.info("session_key: " + session_key);
          LOGGER.info("openid: " + openid);
//          返回session_key和openid
          routingContext.response().putHeader("content-type", "application/json").end(Json.encode(handle.result().bodyAsJsonObject()));
        }
      });
  }

  private void index(RoutingContext routingContext) {
    routingContext.end("hello from vertx");
  }

  /**
   * 调用 python 服务
   *
   * @param data
   * @return
   */
  private Future<Void> callPython(JsonObject data) {
    Promise<Void> promise = Promise.promise();
    DeliveryOptions deliveryOptions = new DeliveryOptions();
//    计时2分钟
    deliveryOptions.setSendTimeout(120000);
    eventBus.request("Webclient.bus", data, deliveryOptions, reply -> {
      System.out.println("have received a reply from webclientVerticle");
      System.out.println(reply.result());
      System.out.println("reply succeeded?:" + reply.succeeded());
      if (reply.succeeded()) {
        promise.complete();
      } else {
        promise.fail(reply.cause());
      }
    });
    return promise.future();
  }

  /**
   * 保存路径到数据库
   *
   * @param data
   * @return
   */
  private Future<Void> store_Path(JsonObject data) {
    Promise<Void> promise = Promise.promise();
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "store_path");
    eventBus.request("mysql.bus", data, options, reply -> {
      if (reply.succeeded()) {
        promise.complete();
      } else {
        promise.fail(reply.cause());
      }
    });
    return promise.future();
  }

  /**
   * 接收图片
   *
   * @param routingContext
   */
  // TODO: 2022/2/17 test
  private void image(RoutingContext routingContext) {
    LOGGER.info("into http server of image");
    //    1、标准图片索引
    String index = routingContext.request().getParam("index");
    HttpVerticle.uploadPIC_rootPath = System.getProperty("user.dir").toString();
    LOGGER.info("current path is " + HttpVerticle.uploadPIC_rootPath);

    Set<FileUpload> uploads = routingContext.fileUploads();
    Iterator<FileUpload> it = uploads.iterator();
    LOGGER.info("the size of upload file: " + uploads.size());

    while (it.hasNext()) {
      //    2、生成唯一 uuid
      String uuid = UUID.randomUUID().toString();
      LOGGER.info("the uuid of file: " + uuid);

      //    3、移动临摹图片到相应的文件夹下，并设置文件名为uuid
      FileUpload fileUpload = it.next();
      String new_fileName = uuid + ".jpg";
      LOGGER.info("filename: " + fileUpload.fileName());
      LOGGER.info("uploadedFileName: " + fileUpload.uploadedFileName());

      //    4、寻找出对应的标准图片路径
      String zitie_path = uploadPIC_rootPath + "/stand_pictures/" + index + ".jpg";
      String linmo_path = uploadPIC_rootPath + "/test/" + new_fileName;
      String diff_path = uploadPIC_rootPath + "/result/" + new_fileName;
      vertx.fileSystem().move(fileUpload.uploadedFileName(), uploadPIC_rootPath + "/test/" + new_fileName, res -> {
        if (res.succeeded()) {
          //    5、将对应的临摹图片，标准图片保存到数据库中
          JsonObject data = new JsonObject()
            .put("zitie_path", zitie_path)
            .put("linmo_path", linmo_path)
            .put("diff_path", diff_path)
            .put("uuid", uuid);
          JsonObject data2 = new JsonObject().put("uuid", uuid);
          store_Path(data).onSuccess(res2 -> {
            LOGGER.info("store path to mysql success");
            it.remove();
            //  6、调用python服务
            callPython(data2).onSuccess(res3 -> {
              LOGGER.info("call python success");
              this
                .assess_info_insert(openid, uuid, index)
                .onSuccess(
                  res4 -> {
                    JsonObject result = new JsonObject().put("err_code", 0).put("data", new JsonObject().put("uuid", uuid).put("index", index)).put("msg", "success");
                    routingContext.response().putHeader("content-type", "application/json").end(result.encode());
                  });
            });
          });
        } else {
          JsonObject result = new JsonObject().put("err_code", 1).put("data", uuid).put("msg", "fail");
          routingContext.response().putHeader("content-type", "application/json").end(result.encode());
        }
      });
    }
  }

  /**
   * 保存信息到 assess_info_insert
   *
   * @param openid
   * @param uuid
   * @param id
   * @return
   */
  private Future<Void> assess_info_insert(String openid, String uuid, String id) {
    Promise<Void> promise = Promise.promise();
    JsonObject data = new JsonObject();
    data
      .put("openid", openid)
      .put("uuid", uuid)
      .put("id", id);
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "assess_info_insert"), reply -> {
      if (reply.succeeded()) {
        promise.complete();
      } else {
        promise.fail(reply.cause());
      }
    });
    return promise.future();
  }

  private void assess_info_getbyid(RoutingContext routingContext) {
    String id = routingContext.request().getParam("id");
    JsonObject data = new JsonObject();
    data.put("id", id);
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "assess_info_getbyid"), reply -> {
      if (reply.succeeded()) {
        JsonObject result = (JsonObject) reply.result().body();
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(result.encodePrettily());
      } else {
        routingContext
          .response()
          .putHeader("content-type", "application/json")
          .end(new JsonObject().put("err_code", 0).put("message", reply.cause().toString()).encodePrettily());
      }
    });
  }


  /**
   * delete linmo.jpg
   *
   * @return
   */
  private Future<Void> exits_linmo() {
    String new_fileName = "linmo.jpg";
    Promise<Void> promise = Promise.promise();
    vertx.fileSystem().exists(uploadPIC_rootPath + "/" + new_fileName, res1 -> {
      if (res1.succeeded() && res1.result()) {
        vertx.fileSystem().delete(uploadPIC_rootPath + "/" + new_fileName, res2 -> {
          System.out.println("File deleted");
          promise.complete();
        });
      } else {
        promise.complete();
      }
    });
    return promise.future();
  }

  /**
   * delete template.jpg
   *
   * @return
   */
  private Future<Void> exits_template() {
    String new_fileName = "template.jpg";
    Promise<Void> promise = Promise.promise();
    vertx.fileSystem().exists(uploadPIC_rootPath + "/" + new_fileName, res1 -> {
      if (res1.succeeded() && res1.result()) {
        vertx.fileSystem().delete(uploadPIC_rootPath + "/" + new_fileName, res2 -> {
          System.out.println("File deleted");
          promise.complete();
        });
      } else {
        promise.complete();
      }
    });
    return promise.future();
  }

  /**
   * delete wan_mark.jpg
   *
   * @return
   */
  private Future<Void> exits_wan_mark() {
    String new_fileName = "wan_mark.jpg";
    Promise<Void> promise = Promise.promise();
    vertx.fileSystem().exists(uploadPIC_rootPath + "/" + new_fileName, res1 -> {
      if (res1.succeeded() && res1.result()) {
        vertx.fileSystem().delete(uploadPIC_rootPath + "/" + new_fileName, res2 -> {
          System.out.println("File deleted");
          promise.complete();
        });
      } else {
        promise.complete();
      }
    });
    return promise.future();
  }

  /**
   * copy the template pic to the assigned file
   *
   * @param index
   * @return
   */
  private Future<Void> copyTemplatePIC(String index) {
    Promise<Void> promise = Promise.promise();
    CopyOptions options = new CopyOptions();
    vertx.fileSystem().copy(System.getProperty("user.dir").toString() + "/stand_pictures/" + index + ".jpg"
        , System.getProperty("user.dir").toString() + "/pictures/template.jpg")
      .onComplete(ar -> {
        if (ar.succeeded()) {
          System.out.println("success");
          System.out.println(ar.result());
          promise.complete();
        } else {
          System.out.println(ar.cause());
          promise.fail(ar.cause());
        }
      });
    return promise.future();
  }

}
