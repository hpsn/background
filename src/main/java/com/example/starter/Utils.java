package com.example.starter;

import io.vertx.core.Future;
import io.vertx.core.Promise;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

/**
 * @Author super_b
 * @Date 2022/1/26 10:43
 * @Version 1.0
 */
public class Utils {

  /**
   * 构造函数
   */
  public Utils() {

  }

  /**
   * 获取数据库语句
   *
   * @return
   */
  public static Future<HashMap> getDBqueries() {
    //    使用HashMap提高查找效率
    Promise<HashMap> promise = Promise.promise();
    HashMap<db_queries, String> res = new HashMap<>();
    Properties properties = new Properties();
    BufferedReader bufferedReader = null;
    try {
      // TODO: 2022/1/28 windows和linux下的绝对路径不一致
//      bufferedReader = new BufferedReader(new FileReader("/root/db_queries.properties"));
      bufferedReader = new BufferedReader(new FileReader("D:\\hpsn\\hpsn back-up\\src\\main\\resources\\db_queries.properties"));
      properties.load(bufferedReader);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    for (db_queries item : db_queries.values()) {
      res.put(item, properties.getProperty(item.toString()));
    }
    promise.complete(res);
    return promise.future();
  }


}
