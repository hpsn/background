package com.example.starter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.impl.logging.Logger;
import io.vertx.core.impl.logging.LoggerFactory;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

import java.util.UUID;


public class WebclientVerticle extends AbstractVerticle {

  private static final Logger LOGGER = LoggerFactory.getLogger(WebclientVerticle.class);
  private static final String URI = "http://106.52.28.79/assess/";
  private static WebClient webClient = null;
  private static final String eventBusName = "Webclient.bus";

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    EventBus eventBus = vertx.eventBus();
    eventBus.consumer(eventBusName, message -> {
      handleMessage(message);
    });
    webClient = WebClient.create(vertx);

  }

  private void handleMessage(Message tMessage){
    JsonObject data = new JsonObject();
    data = (JsonObject) tMessage.body();
    sendRequest(data.getString("uuid")).onComplete(ar -> {
      if(ar.succeeded()){
        System.out.println("发送请求成功");
        tMessage.reply("request successfully");
      }else{
        tMessage.fail(0, "发送请求失败");
      }
    });
  }


  /**
   * send a request to python service
   *
   * @param UUID
   */
  private Future<Void> sendRequest(String UUID) {
    System.out.println("开始发送request");
    Promise<Void> promise = Promise.promise();
    webClient
      .getAbs("http://106.52.28.79/assess/" + UUID)
      .send()
      .onSuccess(response -> {
//        System.out.println("发送请求成功");
        promise.complete();
      })
      .onFailure(arr -> {
//        System.out.println("发送请求失败");
        LOGGER.error(arr);
        promise.fail(arr.toString());
      })
      .onComplete(complete -> {
        System.out.println(complete.result().body());
      });
    return promise.future();
  }

}
