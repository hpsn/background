package com.example.starter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Promise;

public class MainVerticle extends AbstractVerticle {

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    vertx.deployVerticle("com.example.starter.HttpVerticle", new DeploymentOptions().setInstances(5)).onSuccess(ar1 -> {
      System.out.println("deploy HttpVerticle successfully");
      vertx.deployVerticle("com.example.starter.MysqlVerticle", new DeploymentOptions().setInstances(5)).onSuccess(ar2 -> {
        System.out.println("deploy MysqlVerticle successfully");
        vertx.deployVerticle("com.example.starter.WebclientVerticle", new DeploymentOptions().setInstances(5)).onSuccess(ar3 -> {
          System.out.println("deploy WebclientVerticle successfully");
        });
      });
    });
  }
}
