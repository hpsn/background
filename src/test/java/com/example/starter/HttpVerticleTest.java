package com.example.starter;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.file.FileSystemOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Author super_b
 * @Date 2022/1/27 9:54
 * @Version 1.0
 */
@ExtendWith(VertxExtension.class)
class HttpVerticleTest {
  WebClient webClient = null;
  Vertx vertx = null;
  EventBus eventBus = null;

  @BeforeEach
  @Test
  @DisplayName("prepare")
  void prepare(VertxTestContext vertxTestContext) {
    vertx = Vertx.vertx(new VertxOptions()
      .setMaxEventLoopExecuteTime(1000)
      .setPreferNativeTransport(true)
      .setFileSystemOptions(new FileSystemOptions().setFileCachingEnabled(true)));
    WebClientOptions webClientOptions = new WebClientOptions().setDefaultPort(3306);
    webClient = WebClient.create(vertx, webClientOptions);
    eventBus = vertx.eventBus();
    DeploymentOptions deploymentOptions = new DeploymentOptions();
    deploymentOptions.setInstances(1);
    vertx.deployVerticle(new HttpVerticle(), deploymentOptions, handler -> {
      if (handler.succeeded()) {
        vertx.deployVerticle(new MysqlVerticle(), handler1 -> {
          if (handler1.succeeded()) {
            vertxTestContext.completeNow();
          } else {
            vertxTestContext.failNow(handler1.cause());
          }
        });
      } else {
        vertxTestContext.failNow(handler.cause());
      }
    });
  }

  @Test
  @DisplayName("test zitie_collect_add")
  void test_zitie_collect_add(VertxTestContext vertxTestContext) {
    webClient
      .postAbs("http://localhost:8888/zitie_collect/add")
      .sendJsonObject(
        new JsonObject().put("openid", "236666").put("id", "00007"),
        ar2 -> {
          if (ar2.succeeded()) {
            System.out.println(ar2.result().body());
            assertEquals(0, ar2.result().bodyAsJsonObject().getInteger("error_code"));
            assertEquals("success", ar2.result().bodyAsJsonObject().getString("message"));
            vertxTestContext.completeNow();
          } else {
            vertxTestContext.failNow(ar2.cause());
          }
        });
  }

  @Test
  @DisplayName("zitie_collect_delete")
  void test_zitie_collect_delete(VertxTestContext vertxTestContext) {
    webClient
      .postAbs("http://localhost:8888/zitie_collect/delete")
      .sendJsonObject(
        new JsonObject().put("openid", "789654321").put("id", "857412"),
        ar2 -> {
          if (ar2.succeeded()) {
            assertEquals(0, ar2.result().bodyAsJsonObject().getInteger("error_code"));
            assertEquals("success", ar2.result().bodyAsJsonObject().getString("message"));
            vertxTestContext.completeNow();
          } else {
            vertxTestContext.failNow(ar2.cause());
          }
        });
  }

  @Test
  @DisplayName("zitie_collect_get")
  void test_zitie_collect_get(VertxTestContext vertxTestContext) {
    webClient
      .getAbs("http://localhost:8888/zitie_collect/get")
      .addQueryParam("openid", "18312834124")
      .send(ar2 -> {
        if (ar2.succeeded()) {
          assertEquals(0, ar2.result().bodyAsJsonObject().getInteger("error_code"));
          assertEquals("success", ar2.result().bodyAsJsonObject().getString("message"));
          System.out.println(ar2.result().bodyAsJsonObject().getJsonArray("data").toString());
          vertxTestContext.completeNow();
        } else {
          vertxTestContext.failNow(ar2.cause());
        }
      });
  }

  @Test
  @DisplayName("zitie_collect_exist")
  void Test_zitie_collect_exist(VertxTestContext vertxTestContext) {
    webClient
      .getAbs("http://localhost:8888/zitie_collect/exist")
      .addQueryParam("openid", "18312834124")
      .addQueryParam("id", "2")
      .send(ar -> {
        if (ar.succeeded()) {
          System.out.println(ar.result().body().toString());
          vertxTestContext.completeNow();
        } else {
          vertxTestContext.failNow(ar.cause());
        }
      });
  }

  @Test
  @DisplayName("zitie_collect_exist_total")
  void Test_zitie_collect_exist_total(VertxTestContext vertxTestContext) {
    webClient
      .getAbs("http://localhost:8888/zitie_collect/exist/total")
      .addQueryParam("openid", "18312834124")
      .send(ar -> {
        if (ar.succeeded()) {
          System.out.println(ar.result().body().toString());
          vertxTestContext.completeNow();
        } else {
          vertxTestContext.failNow(ar.cause());
        }
      });
  }

  @Test
  @DisplayName("shufa_collect_add")
  void Test_shufa_collect_add(VertxTestContext vertxTestContext) {
    webClient
      .postAbs("http://localhost:8888/shufa_collect/add")
      .sendJsonObject(
        new JsonObject().put("openid", "8848").put("id", "3"),
        ar2 -> {
          if (ar2.succeeded()) {
            assertEquals(0, ar2.result().bodyAsJsonObject().getInteger("error_code"));
            assertEquals("success", ar2.result().bodyAsJsonObject().getString("message"));
            vertxTestContext.completeNow();
          } else {
            vertxTestContext.failNow(ar2.cause());
          }
        });
  }

  @Test
  @DisplayName("shufa_collect_delete")
  void Test_shufa_collect_delete(VertxTestContext vertxTestContext) {
    webClient
      .postAbs("http://localhost:8888/shufa_collect/delete")
      .sendJsonObject(
        new JsonObject().put("openid", "8848").put("id", "3"),
        ar2 -> {
          if (ar2.succeeded()) {
            assertEquals(0, ar2.result().bodyAsJsonObject().getInteger("error_code"));
            assertEquals("success", ar2.result().bodyAsJsonObject().getString("message"));
            vertxTestContext.completeNow();
          } else {
            vertxTestContext.failNow(ar2.cause());
          }
        });
  }

  @Test
  @DisplayName("shufa_collect_get")
  void Test_shufa_collect_get(VertxTestContext vertxTestContext) {
    webClient
      .getAbs("http://localhost:8888/shufa_collect/get")
      .addQueryParam("openid", "8848")
      .send(ar2 -> {
        if (ar2.succeeded()) {
          assertEquals(0, ar2.result().bodyAsJsonObject().getInteger("error_code"));
          assertEquals("success", ar2.result().bodyAsJsonObject().getString("message"));
          System.out.println(ar2.result().bodyAsJsonObject().getJsonArray("data").toString());
          vertxTestContext.completeNow();
        } else {
          vertxTestContext.failNow(ar2.cause());
        }
      });
  }

  @Test
  @DisplayName("shufa_collect_exist")
  void Test_shufa_collect_exist(VertxTestContext vertxTestContext) {
    webClient
      .getAbs("http://localhost:8888/shufa_collect/exist")
      .addQueryParam("openid", "18312834124")
      .addQueryParam("id", "2")
      .send(ar -> {
        if (ar.succeeded()) {
          System.out.println(ar.result().body().toString());
          vertxTestContext.completeNow();
        } else {
          vertxTestContext.failNow(ar.cause());
        }
      });
  }

  @Test
  @DisplayName("shufa_collect_exist_total")
  void Test_shufa_collect_exist_total(VertxTestContext vertxTestContext) {
    webClient
      .getAbs("http://localhost:8888/shufa_collect/exist/total")
      .addQueryParam("openid", "18312834124")
      .send(ar -> {
        if (ar.succeeded()) {
          System.out.println(ar.result().body().toString());
          vertxTestContext.completeNow();
        } else {
          vertxTestContext.failNow(ar.cause());
        }
      });
  }

  @Test
  @DisplayName("goods_collect_add")
  void Test_goods_collect_add(VertxTestContext vertxTestContext) {
    webClient
      .postAbs("http://localhost:8888/goods_collect/add")
      .sendJsonObject(
        new JsonObject().put("openid", "8848").put("id", "3"),
        ar2 -> {
          if (ar2.succeeded()) {
            assertEquals(0, ar2.result().bodyAsJsonObject().getInteger("error_code"));
            assertEquals("success", ar2.result().bodyAsJsonObject().getString("message"));
            vertxTestContext.completeNow();
          } else {
            vertxTestContext.failNow(ar2.cause());
          }
        });
  }

  @Test
  @DisplayName("goods_collect_delete")
  void Test_goods_collect_delete(VertxTestContext vertxTestContext) {
    webClient
      .postAbs("http://localhost:8888/goods_collect/delete")
      .sendJsonObject(
        new JsonObject().put("openid", "8848").put("id", "3"),
        ar2 -> {
          if (ar2.succeeded()) {
            assertEquals(0, ar2.result().bodyAsJsonObject().getInteger("error_code"));
            assertEquals("success", ar2.result().bodyAsJsonObject().getString("message"));
            vertxTestContext.completeNow();
          } else {
            vertxTestContext.failNow(ar2.cause());
          }
        });
  }

  @Test
  @DisplayName("goods_collect_get")
  void Test_goods_collect_get(VertxTestContext vertxTestContext) {
    webClient
      .getAbs("http://localhost:8888/goods_collect/get")
      .addQueryParam("openid", "8848")
      .send(ar2 -> {
        if (ar2.succeeded()) {
          assertEquals(0, ar2.result().bodyAsJsonObject().getInteger("error_code"));
          assertEquals("success", ar2.result().bodyAsJsonObject().getString("message"));
          System.out.println(ar2.result().bodyAsJsonObject().getJsonArray("data").toString());
          vertxTestContext.completeNow();
        } else {
          vertxTestContext.failNow(ar2.cause());
        }
      });
  }

  @Test
  @DisplayName("goods_collect_exist")
  void Test_goods_collect_exist(VertxTestContext vertxTestContext) {
    webClient
      .getAbs("http://localhost:8888/goods_collect/exist")
      .addQueryParam("openid", "3839")
      .addQueryParam("id", "2")
      .send(ar -> {
        if (ar.succeeded()) {
          System.out.println(ar.result().body().toString());
          vertxTestContext.completeNow();
        } else {
          vertxTestContext.failNow(ar.cause());
        }
      });
  }

  @Test
  @DisplayName("goods_collect_exist_total")
  void Test_goods_collect_exist_total(VertxTestContext vertxTestContext) {
    webClient
      .getAbs("http://localhost:8888/goods_collect/exist/total")
      .addQueryParam("openid", "3839")
      .send(ar -> {
        if (ar.succeeded()) {
          System.out.println(ar.result().body().toString());
          vertxTestContext.completeNow();
        } else {
          vertxTestContext.failNow(ar.cause());
        }
      });
  }

  @Test
  @DisplayName("remark_get_total")
  void Test_remark_get_total(VertxTestContext vertxTestContext) {
    webClient
      .getAbs("http://localhost:8888/remark/total")
      .addQueryParam("openid", "1375632774")
      .send(ar -> {
        if (ar.succeeded()) {
          System.out.println(ar.result().body());
          vertxTestContext.completeNow();
        } else {
          vertxTestContext.failNow(ar.cause());
        }
      });
  }

  @Test
  @DisplayName("remark_get_max_score")
  void Test_remark_get_max_score(VertxTestContext vertxTestContext) {
    webClient
      .getAbs("http://localhost:8888/remark/max/score")
      .addQueryParam("openid", "1375632774")
      .send(ar -> {
        if (ar.succeeded()) {
          System.out.println(ar.result().body());
          vertxTestContext.completeNow();
        } else {
          vertxTestContext.failNow(ar.cause());
        }
      });
  }

  @Test
  @DisplayName("remark_get_min_score")
  void Test_remark_get_min_score(VertxTestContext vertxTestContext) {
    webClient
      .getAbs("http://localhost:8888/remark/min/score")
      .addQueryParam("openid", "3839")
      .send(ar -> {
        if (ar.succeeded()) {
          System.out.println(ar.result().body());
          vertxTestContext.completeNow();
        } else {
          vertxTestContext.failNow(ar.cause());
        }
      });
  }

  @Test
  @DisplayName("remark_get_avg_score")
  void Test_remark_get_avg_score(VertxTestContext vertxTestContext) {
    webClient
      .getAbs("http://localhost:8888/remark/avg/score")
      .addQueryParam("openid", "1375632774")
      .send(ar -> {
        if (ar.succeeded()) {
          System.out.println(ar.result().body());
          vertxTestContext.completeNow();
        } else {
          vertxTestContext.failNow(ar.cause());
        }
      });
  }

  @Test
  @DisplayName("assess_info_getbyid")
  void Test_assess_info_getbyid(VertxTestContext vertxTestContext) {
    webClient
      .getAbs("http://localhost:8888/remark/get/byid")
      .addQueryParam("id", "0009")
      .send(ar -> {
        if (ar.succeeded()) {
          System.out.println(ar.result().body());
          vertxTestContext.completeNow();
        } else {
          vertxTestContext.failNow(ar.cause());
        }
      });
  }


  @AfterEach
  void stop() {
  }
}
