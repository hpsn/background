package com.example.starter;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.file.FileSystemOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 * @Author super_b
 * @Date 2022/1/26 11:09
 * @Version 1.0
 */
@ExtendWith(VertxExtension.class)
class MysqlVerticleTest {
  Vertx vertx = null;
  EventBus eventBus = null;

  @BeforeAll
  static void prepare() {

  }

  @BeforeEach
  @Test
  @DisplayName("deploy MysqlVerticle")
  void Teststart(VertxTestContext vertxTestContext) {
    vertx = Vertx.vertx(new VertxOptions()
      .setMaxEventLoopExecuteTime(1000)
      .setPreferNativeTransport(true)
      .setFileSystemOptions(new FileSystemOptions().setFileCachingEnabled(true)));
    eventBus = vertx.eventBus();
    vertx.deployVerticle(new MysqlVerticle(), handle -> {
      if (handle.succeeded()) {
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(handle.cause());
      }
    });
  }

  @Test
  @DisplayName("zitie_collect_add")
  void Test_zitie_collect_add(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject();
    data
      .put("openid", "18312834124")
      .put("id", "3306");
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "zitie_collect_add");
    eventBus.request("mysql.bus", data, options, reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("zitie_collect_delete")
  void Test_zitie_collect_delete(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject();
    data
      .put("openid", "18312834124")
      .put("id", "3306");
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "zitie_collect_delete");
    eventBus.request("mysql.bus", data, options, reply -> {
      if (reply.succeeded()) {
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("zitie_collect_get")
  void Test_zitie_collect_get(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject();
    data.put("openid", "18312834124");
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "zitie_collect_get");
    eventBus.request("mysql.bus", data, options, reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("zitie_collect_exist")
  void Test_zitie_collect_exits(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject();
    data
      .put("openid", "18312834124")
      .put("id", "2");
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "zitie_collect_exist"), reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("zitie_collect_exist_total")
  void Test_zitie_collect_exist_total(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject().put("openid", "18312834124");
    DeliveryOptions deliveryOptions = new DeliveryOptions();
    deliveryOptions.addHeader("index", "zitie_collect_exist_total");
    eventBus.request("mysql.bus", data, deliveryOptions, reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }


  @Test
  @DisplayName("zitie_collect_get send request 20 times")
  void Test_zitie_collect_get_10(VertxTestContext vertxTestContext) {
    Checkpoint checkpoint = vertxTestContext.checkpoint(20);
    JsonObject data = new JsonObject();
    data.put("openid", "18312834124");
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "zitie_collect_get");
    for (int i = 0; i < 20; i++) {
      System.out.println("the " + i + " time");
      eventBus.request("mysql.bus", data, options, reply -> {
        if (reply.succeeded()) {
          System.out.println(reply.result().body().toString());
          checkpoint.flag();
        } else {
          vertxTestContext.failNow(reply.cause());
        }
      });
    }
  }

  @Test
  @DisplayName("shufa_collect_add")
  void Test_shufa_collect_add(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject();
    data
      .put("openid", "18312834124")
      .put("id", "8699");
    DeliveryOptions options = new DeliveryOptions();
    options.addHeader("index", "shufa_collect_add");
    eventBus.request("mysql.bus", data, options, reply -> {
      if (reply.succeeded()) {
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("shufa_collect_delete")
  void Test_shufa_collect_delete(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject();
    data.put("openid", "18312834124").put("id", "8699");
    DeliveryOptions options = new DeliveryOptions().addHeader("index", "shufa_collect_delete");
    eventBus.request("mysql.bus", data, options, reply -> {
      if (reply.succeeded()) {
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("shufa_collect_get")
  void Test_shufa_collect_get(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject().put("openid", "18312834124");
    DeliveryOptions options = new DeliveryOptions().addHeader("index", "shufa_collect_get");
    eventBus.request("mysql.bus", data, options, reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("shufa_collect_exist")
  void Test_shufa_collect_exits(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject();
    data
      .put("openid", "3839")
      .put("id", "2");
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "shufa_collect_exist"), reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("shufa_collect_exist_total")
  void Test_shufa_collect_exist_total(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject().put("openid", 3839);
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "shufa_collect_exist_total"), reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("goods_collect_add")
  void Test_goods_collect_add(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject().put("openid", "1375632774").put("id", "2333");
    DeliveryOptions options = new DeliveryOptions().addHeader("index", "goods_collect_add");
    eventBus.request("mysql.bus", data, options, reply -> {
      if (reply.succeeded()) {
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("goods_collect_delete")
  void Test_goods_collect_delete(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject().put("openid", "1375632774").put("id", "2333");
    DeliveryOptions options = new DeliveryOptions().addHeader("index", "goods_collect_delete");
    eventBus.request("mysql.bus", data, options, reply -> {
      if (reply.succeeded()) {
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("goods_collect_get")
  void Test_goods_collect_get(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject().put("openid", "1375632774");
    DeliveryOptions options = new DeliveryOptions().addHeader("index", "goods_collect_get");
    eventBus.request("mysql.bus", data, options, reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("goods_collect_exist")
  void Test_goods_collect_exits(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject();
    data
      .put("openid", "3839")
      .put("id", "2");
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "goods_collect_exist"), reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("goods_collect_exist_total")
  void Test_goods_collect_exist_total(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject().put("openid", 3839);
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "goods_collect_exist_total"), reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("remark_get_total having no data")
  void Test_remark_get_total_no(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject().put("openid", "132323");
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "remark_get_total"), reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("remark_get_total having  data")
  void Test_remark_get_total_yes(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject().put("openid", "1375632774");
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "remark_get_total"), reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("remark_get_max_score")
  void Test_remark_get_max_score(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject().put("openid", "1375632774");
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "remark_get_max_score"), reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("remark_get_MIN_score")
  void Test_remark_get_min_score(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject().put("openid", "1375632774");
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "remark_get_min_score"), reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("remark_get_AVG_score")
  void Test_remark_get_AVG_score(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject().put("openid", "1375632774");
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "remark_get_avg_score"), reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("assess_info_insert")
  void Test_assess_info_insert(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject();
    data
      .put("openid", "18312834124")
      .put("uuid", "45656-test-uuid")
      .put("id", "0005");
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "assess_info_insert"), reply -> {
      if (reply.succeeded()) {
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }

  @Test
  @DisplayName("assess_info_getbyid")
  void Test_assess_info_getbyid(VertxTestContext vertxTestContext) {
    JsonObject data = new JsonObject();
    data
      .put("id", "0009");
    eventBus.request("mysql.bus", data, new DeliveryOptions().addHeader("index", "assess_info_getbyid"), reply -> {
      if (reply.succeeded()) {
        System.out.println(reply.result().body().toString());
        vertxTestContext.completeNow();
      } else {
        vertxTestContext.failNow(reply.cause());
      }
    });
  }


}
