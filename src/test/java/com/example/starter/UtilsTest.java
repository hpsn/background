package com.example.starter;

import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Author super_b
 * @Date 2022/1/26 10:46
 * @Version 1.0
 */
@ExtendWith(VertxExtension.class)
class UtilsTest {

  @Test
  @DisplayName("get db_queries SQL")
  void TestgetDBqueries(VertxTestContext vertxTestContext) {
    Utils.getDBqueries().onComplete(ar -> {
      if(ar.succeeded()){
        vertxTestContext.completeNow();
        System.out.println(ar.result().toString());
      }else{
        vertxTestContext.failNow(ar.cause());
      }
    });
  }
}
